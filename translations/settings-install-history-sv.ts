<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv">
<context>
    <name>MainPage</name>
    <message>
        <source>Install History</source>
        <translation>Installationshistorik</translation>
    </message>
    <message>
        <source>%L1 records</source>
        <translation type="vanished">%L1 poster</translation>
    </message>
    <message>
        <source>Jump to…</source>
        <translation type="vanished">Hoppa till…</translation>
    </message>
    <message>
        <source>tap to select</source>
        <translation type="vanished">Tryck för att välja</translation>
    </message>
    <message>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>Namn</translation>
    </message>
    <message>
        <source>hide search</source>
        <translation type="vanished">Dölj sök</translation>
    </message>
    <message>
        <source>Search on %1</source>
        <translation>Sök på %1</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Installerad</translation>
    </message>
    <message>
        <source>Removed</source>
        <translation>Borttagen</translation>
    </message>
    <message>
        <source>n/a</source>
        <translation>Ej tillämpligt</translation>
    </message>
    <message>
        <source>Package Name</source>
        <translation type="vanished">Paketnamn</translation>
    </message>
    <message>
        <source>Repository</source>
        <translation>Förrådsplats</translation>
    </message>
    <message>
        <source>Package</source>
        <translation>Paket</translation>
    </message>
    <message>
        <source>Hide search</source>
        <translation>Dölj sökfältet</translation>
    </message>
    <message>
        <source>Search by Date</source>
        <translation>Sök efter datum</translation>
    </message>
    <message>
        <source>Search by Name</source>
        <translation>Sök efter namn</translation>
    </message>
    <message>
        <source>Verbose Display</source>
        <translation>Utförlig visning</translation>
    </message>
    <message>
        <source>Reduced Display</source>
        <translation>Reducerad visning</translation>
    </message>
    <message>
        <source>local</source>
        <comment>name for a local repo</comment>
        <translation>Lokalt</translation>
    </message>
    <message>
        <source>local</source>
        <comment>type of a local repo</comment>
        <translation>Lokalt</translation>
    </message>
    <message numerus="yes">
        <source>%Ln event(s)</source>
        <comment>very, very unlikely to have only one, still, plurals please!</comment>
        <translation>
            <numerusform>%Ln händelse</numerusform>
            <numerusform>%Ln händelser</numerusform>
        </translation>
    </message>
    <message>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Executes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StatsPage</name>
    <message>
        <source>Install History</source>
        <translation type="unfinished">Installationshistorik</translation>
    </message>
    <message>
        <source>(%L1%)</source>
        <comment>percentage in parentheses, best translate as &apos;(%L1%)&apos;</comment>
        <translation>(%L1%)</translation>
    </message>
    <message>
        <source>View repositories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Crunching Numbers…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Percentages are relative to all recorded installation events. Bars are relative to the item with the highest event count.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>others</source>
        <comment>things that don&apos;t fit in a category</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%L1</source>
        <comment>number of events</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Most active packages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Most active Repositories</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
